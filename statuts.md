# Statuts de l’association MIAGE Connection
**MIAGE Connection, Association étudiante à but non lucratif déclarée sous le régime de la loi du 1er Juillet 1901 et du décret du 16 Août 1901. Association créée en 2004.**


## Titre 1 – DISPOSITIONS GENERALES
### Article A.1 – La Constitution
Il est fondé, conformément aux dispositions de la loi du 1er Juillet 1901 et de ses décrets d’application, une association dénommée MIAGE Connection qui fédère les associations étudiantes, les élus étudiants et les associations de diplômés de la filière universitaire de Méthodes Informatiques Appliquées à la Gestion des Entreprises (MIAGE).

### Article A.2 – Le siège social
Son siège social est fixé à la FAGE (Fédération des Associations Générales Etudiantes) : 79, rue Perier - 92120 Montrouge.

Il ne peut être transféré que sur décision de l’Assemblée Générale réunie et statuant conformément aux dispositions des présents statuts.

### Article A.3 – La durée
L’Association MIAGE Connection est fondée pour une durée illimitée. Seule une décision judiciaire, administrative ou de l’Assemblée Générale prise conformément aux dispositions prévues par les présents statuts peut entraîner sa dissolution.

### Article A.4 – L’objet
L’association MIAGE Connection a pour objet :

- De représenter et de défendre les droits et intérêts matériels et moraux, tant collectifs qu’individuels des étudiants de la filière MIAGE, et d’exprimer leur(s) position(s) sur tous les sujets les concernant auprès des pouvoirs publics et de l’opinion,
- De promouvoir le diplôme MIAGE auprès des futurs étudiants, des institutions et du monde professionnel,
- De contribuer au développement de toute action permettant de favoriser l’insertion professionnelle des étudiants en MIAGE,
- De développer les liens, les échanges, la coopération et la solidarité entre ses associations membres,
- De promouvoir l’esprit d’association, l’esprit d’entreprendre et de l’esprit de solidarité chez les étudiants de MIAGE,
- De réaliser des actions pour former et informer les étudiants et diplômés en MIAGE et leurs représentants dans les instances de l’enseignement supérieur.

### Article A.5 – Les caractères
L’association MIAGE Connection est ouverte à tous les étudiants et diplômés de la filière MIAGE sans distinction de race, de sexe, de nationalité, de religion ou d’opinion. Elle agit indépendamment de tout groupement partisan ou confessionnel et s’interdit toute prise de position partisane ou confessionnelle.

## Titre 2 – COMPOSITION

### Article A.1 – Les membres de l’association
La fédération se compose de plusieurs catégories de membres :

- Collège A,
- Collège B,
- Collège C,
- Membres d’honneur.

### Article A.2 – Le collège A
Le collège A est composé des associations d’étudiants issus de la filière MIAGE et désirant se regrouper sur le plan national.

Toute nouvelle adhésion sera soumise à l’acceptation du Conseil d’Administration, et sera définitive à compter de l’Assise suivante. La démarche exacte d’adhésion est précisée dans le règlement intérieur.

Elles possèdent une voix délibérative aux assises de MIAGE Connection.
Pour être qualifié d’association membre du collège A, il faut :

- Etre et rester une association étudiante, indépendante de tout parti politique, syndicat et religion,
- Ne pas aller à l’encontre des buts poursuivis par MIAGE Connection, fixés par l’article 1.04 des présents statuts,
- S’acquitter de la cotisation annuelle fixée par le règlement intérieur,
- Pour une association de type Junior-Entreprise, avoir inscrit dans ses statuts que son conseil d'administration est composé intégralement d'étudiants issus de la filière MIAGE.

### Article A.3 – Le collège B
Le collège B est composé des associations d’anciens étudiants issus de la filière MIAGE et désirant se regrouper sur le plan national. 

Toute nouvelle adhésion sera soumise à l’acceptation du Conseil d’Administration, et sera définitive à compter de l’Assise suivante. La démarche exacte d’adhésion est précisée dans le règlement intérieur.

Elles possèdent une voix consultative aux assises de MIAGE Connection.

Pour être qualifié d’association membre du collège B, il faut :

- Etre et rester une association indépendante de tout parti politique, syndicat et religion,
- Ne pas aller à l’encontre des buts poursuivis par MIAGE Connection, fixés par l’article 1.04 des présents statuts,
- S’acquitter de la cotisation annuelle fixée par le règlement intérieur.

### Article A.4 – Le collège C 

Le collège C est un membre adhérent permanent qui se compose d’élus ayant candidaté et ayant été acceptés par le Conseil d’Administration. 

Les élus du collège C, ainsi que les élus recensés à  MIAGE Connection, ont la responsabilité d’informer le bureau national de tout changement de situation de son/ses statut(s) de représentant étudiant dans les instances universitaires et/ou CROUS. 

Les membres de ce collège conservent ce statut jusqu’à la fin de leur mandat ou :

- s’ils demandent leur retrait anticipé
- si le collège décide d’exclure une personne sous justification présentée au Conseil d’Administration
- s’ils sont membres d’une association partenaire de MIAGE Connection ou d’une association allant à l’encontre des objets de MIAGE Connection

Il possède une voix consultative lors des Assises de MIAGE Connection. 

Le collège C de MIAGE Connection a pour objet : 

- La création d’une cellule d’expertise sur les sujets d’affaires académiques et sur l’enseignement supérieur capable d’apporter des conseils aux membres du réseau MIAGE Connection. 
- De développer les affaires académiques au sein de la fédération MIAGE Connection et du réseau MIAGE.
- De donner aux élus étudiants issus de la formation MIAGE une reconnaissance officielle au sein de MIAGE Connection et de son réseau. 

### Article A.5 – Les membres d’honneur
Sont membres d’honneur, les personnes physiques ou morales ayant rendu de grands services pour le développement de MIAGE Connection. Leur élection, proposée par le bureau, est soumise au vote de l’Assemblée Générale.

Ils possèdent une voix consultative lors des assises de MIAGE Connection ; ils peuvent être invités à ces assises sur demande du bureau.

Ils ne doivent pas s’acquitter de la cotisation annuelle.

### Article A.6 – La perte de qualité de membre
La qualité de membre se perd :

- Par démission notifiée par écrit au président et ratifiée par le bureau,
- Par radiation prononcée par le Conseil d’Administration (CA), pour motif grave, à la majorité qualifiée des ¾ des membres présents. Il s’en suivra l’envoi d’une lettre recommandée invitant le représentant de l’association membre à se présenter devant le bureau afin que celui-ci statue. En cas d’absence non justifiée, l’exclusion de l’association membre sera prononcée,
- Par disparition ou par cessation d’activité constatée. Elle est prononcée par le Conseil d’Administration après avis du Bureau National.
- Par exclusion prononcée par le Conseil d’Administration à la majorité qualifiée par perte des qualités requises pour être membre de MIAGE Connection comme énoncées dans les articles 2.02 et 2.03.
- Par exclusion prononcée par le Conseil d’Administration à la majorité qualifiée pour non paiement de la cotisation pendant 2 années consécutives.

### Article A.7 - Les membres du Comité d’Orientation Stratégique
Le Comité d’Orientation Stratégique se compose de trois personnes physiques au maximum, ayant candidaté et ayant été voté par les membres votants qualifiés de la fédération à la majorité simple lors d'une assise. Leur candidature est au préalable ratifiée par le Bureau National avant présentation en assise.

Le Comité d’Orientation Stratégique de MIAGE Connection a pour objectif d’accompagner le Bureau National de par leurs précédentes expériences dans une démarche qualité suivant les règles, de pérennisation et d’évolution des actions de la fédération. Il a un également un rôle de facilitateur dans les démarches.

Pour être qualifié de membre du Comité d’Orientation Stratégique, il faut :
- Ne pas être un administrateur ou membre du Bureau National de MIAGE Connection, ni chargé de mission pour MIAGE Connection,
- Ne pas être un administrateur ou membre du Bureau National de toute Fédération à laquelle MIAGE Connection est adhérente ou partenaire.

Les membres du Comité d’Orientation Stratégique sont élus pour une durée de trois ans maximum, au delà un renouvellement de candidature est nécessaire et obligatoire. Les membres du Comité d’Orientation Stratégique sont dispensés de cotisation et n’ont pas le droit de vote en Conseil d’Administration. La destitution de l’ensemble du Comité d’Orientation Stratégique doit être soutenue à minima par 75% du nombre total des membres de tous les membres votants qualifiés de la fédération.

## Titre 3 – ADMINISTRATION
### Article A.1 – Les organes directeurs
Les organes directeurs de MIAGE Connection sont :
- L’Assemblée Générale (AG),
- Le Conseil d’Administration (CA),
- Le Bureau National (BN).

### Article A.2 – Représentation
Le représentant légal de l’association membre est de droit son représentant au sein des assises de MIAGE Connection. En cas d’empêchement, un membre élu de son bureau est de droit, représentant de l’association.

Le représentant du collège C est un élu de ce collège choisi lors de l’événement pour représenter cette structure, et déclaré auprès du Secrétaire Général de MIAGE Connection au plus tard avant le début de l’assise. Dans le cas de vote à distance, cette personne est choisie par le collège C et est communiquée au Secrétaire Général par courrier ou par courriel 2 jours avant le vote. 
 
### Article A.3 – Procuration
Une association adhérente, si elle ne peut assister ou maintenir sa présence à l’assise, peut donner procuration à une personne physique. Cette procuration est valable à partir du moment où elle a été reçue par le secrétaire général de MIAGE Connection et ce jusqu’à la fin de l’assise.

Une personne physique ne peut détenir plus d’une procuration.

Le représentant du collège C ne peut donner procuration qu’à un autre élu de son collège.

### Article A.4 – Autres participants
Le bureau, s’il le juge nécessaire, peut proposer par la voix du Président la présence et la participation de toute autre personne physique ou morale pour la tenue des débats.

### Article A.5 – Convocation
Les convocations aux assises sont réalisées par courriel et/ou par courrier envoyé à toutes les associations membres au moins quinze jours avant la date retenue.

Les convocations doivent comporter, sous peine de nullité, la date de début et de fin, l’heure, le lieu et l’ordre du jour de la séance.

L’ordre du jour de la convocation est fixé par le Bureau National.

Tout membre peut adresser au Secrétaire Général, par courrier ou courriel au moins sept jours avant la réunion d’une Assemblée Générale, un point complémentaire à l’ordre du jour proposé.
### Article A.6 – Votes
Tous les votes concernant des personnes physiques et donc en particulier l’élection du Bureau National, sont effectués à bulletin secret.

Les autres votes se feront à main levée. Cependant, tout scrutin se fera à bulletin secret sur demande expresse d’au moins deux membres.

Chaque association membre votante représentée dispose d’une voix unique au moment des votes.

Avant la procédure de vote, si au moins un membre du collège B en fait la demande sans qu'aucun membre du collège A ne s'y oppose, une voix délibérative est attribuée aux membres du collège B. Sinon, un vote préliminaire pourra être effectué pour cette décision. Cette procédure s'applique également à l'attribution de voix délibérative au collège C. Dans le cas d'un vote préliminaire, ces voix délibératives exceptionnelles sont acquises par la majorité qualifiée des membres du Collège A.
### Article A.7 – Sessions
Au moins quatre assises ont lieu chaque année.
Il faut au moins l’Assemblée Générale Ordinaire et au moins un Conseil d’Administration. Les autres assises sont choisies selon l’ordre du jour.
## Titre 4 – L’Assemblée Générale
### Article A.1 – Composition
L’Assemblée Générale est l’organe stratégique de l’Association MIAGE Connection.
Le Congrès d’Hiver est le nom donné à l’Assemblée Générale Ordinaire élective.
Font partie de l’Assemblée Générale :
Les représentants de chaque association membre, 
Les représentants du collège C,
Les membres du Bureau National.
### Article A.2 – Quorum
L’Assemblée Générale ne peut siéger valablement que si la moitié des associations ayant la qualité de membre du collège A sont présentes ou représentées à l’ouverture de la séance.
Au cas où le quorum ne serait pas atteint, une nouvelle Assemblée Générale est convoquée en laissant un délai d’au moins deux semaines. Celle-ci délibère valablement quel que soit le nombre d’associations ayant la qualité de membre du collège A présentes ou représentées.
### Article A.3 – Fonctions
L’Assemblée Générale a pour principales fonctions :
- D’entendre tout rapport sur la situation morale et financière de l’association MIAGE Connection, de délibérer sur les questions des membres du bureau sortant,
- De constater les éventuelles démissions et radiations,
- De désigner les membres d’honneur,
- De discuter les questions à l’ordre du jour,
-   D’adopter les éventuelles motions proposées par les membres, qui serviront d’orientation au Conseil d’Administration et au bureau pour les actions à mener et les décisions à prendre,
- D’élire tout ou partie du bureau,
- Seuls les points à l’ordre du jour seront soumis au vote de l’Assemblée Générale.
### Article A.4 – Sessions
L’Assemblée Générale se réunit :
- En session ordinaire, une fois par année universitaire. Elle donne lieu à l’élection du nouveau bureau et éventuellement à la modification des présents statuts et/ou du règlement intérieur,
- En session extraordinaire, à toute époque de l’année sur décision du bureau ou sur demande d’au moins 50% des membres du collège du A, et en avertissant tous les membres de l’association, notamment pour le remplacement d’un ou plusieurs membres du bureau ou pour la modification des présents statuts.
## Titre 5 – Le Conseil d’Administration
### Article A.1 – Composition
Le Conseil d’Administration est l’organe tactique de l’association MIAGE Connection.
Font partie du Conseil d’Administration :
Le représentant de chaque association membre,
Les représentants du collège C,
Les membres du Bureau National.
### Article A.2 – Quorum
Le Conseil d’Administration ne peut siéger valablement que si le tiers des associations ayant la qualité de membre du collège A sont présentes ou représentées à l’ouverture de la séance.

Au cas où le quorum ne serait pas atteint, un nouveau Conseil d’Administration est convoqué en laissant un délai d’au moins deux semaines. Celui-ci délibère valablement quel que soit le nombre d’associations ayant la qualité de membre du collège A présentes ou représentées.

### Article A.3 – Fonctions
Le Conseil d’Administration a pour principales fonctions :
- De mettre en place les orientations proposées par l’Assemblée Générale, en définissant les projets qui seront réalisés par le bureau,
- De discuter les questions à l’ordre du jour,
- De créer toute commission ou groupe de travail nécessaire aux missions de l’association,
- De prendre toute décision qui n’est pas réservée à l’Assemblée Générale.
### Article A.4 – Conseil d’Administration à distance
Le Bureau National de MIAGE Connection peut procéder à la convocation d’un Conseil d’Administration à distance via Internet. Pour le vote de la ville organisatrice et/ou de la date des différents événements, le Bureau National de MIAGE Connection peut procéder à la convocation d’un conseil d’administration à distance via Internet.

**Restrictions** : pour délibérer valablement, le Conseil d’Administration à distance doit réunir la moitié des membres votants en leur qualité de votant.

Le PV de ce Conseil d’Administration devra obligatoirement être soumis au vote du Conseil d’Administration ou de l’Assemblée Générale suivante.

Le détail des modalités et de la procédure de vote est explicité dans le Règlement Intérieur.
## Titre 6 – Le Bureau National
### Article A.1 – Composition
Le Bureau National est l’organe opérationnel de MIAGE Connection.
Il est composé uniquement d’étudiants ou diplômés issus de la filière MIAGE. Il est placé sous la direction d’un président qui doit être étudiant au moment de sa prise de fonction. Le Trésorier doit également être étudiant au moment de sa prise de mandat.
Le Bureau National est composé des membres suivants :
- Un Président,
- Un Trésorier,
- Un Secrétaire Général,
- Au moins un Vice-Président.

Les Vice-Présidents secondent le Président. Le Président choisit parmi eux, un premier Vice-Président. Le Bureau National peut faire appel à des chargés de mission pour des dossiers particuliers pour une durée déterminée. A ce titre, ces chargés de mission peuvent être invités en réunion de bureau ou au Conseil d’Administration pour rendre compte des résultats de leurs travaux.
### Article A.2 – Election du Bureau National
L’élection des membres s’effectue par liste de candidats répartis par postes lors d’une Assemblée Générale. L’élection est acquise à la majorité absolue des membres votants.

En cours de mandat, pour compléter le Bureau ou remplacer des membres démissionnaires, le Bureau National peut coopter un nouveau membre sur proposition du Président, sous réserve que cette cooptation soit approuvée par l’Assemblée Générale à une majorité identique à la majorité requise pour l’élection du Bureau National.

Le Bureau National est élu jusqu’au prochain Congrès National, il se réunit aussi souvent que nécessaire.

Les membres sortants, qui ont obtenu quitus de leur gestion, sont rééligibles s’ils satisfont aux dispositions des présents statuts.

Un membre du Bureau National ne peut être président d’une association adhérente à MIAGE Connection ou son représentant temporaire lors d’une assise de MIAGE Connection. Si c’est le cas, il a un mois pour démissionner de l’une de ces deux fonctions, sans quoi il perd automatiquement son siège au Bureau National. Le Président n’est rééligible qu’une seule fois.
### Article A.3 – Fonctions
Le Bureau National a pour fonctions :
De veiller au respect des présents statuts ;
De représenter l’Association MIAGE Connection ;
De mettre en oeuvre les projets définis par le Conseil d’Administration :
- La prise de toutes décisions nécessaires à la bonne marche de l’association en accord avec le Conseil d’Administration
- De convoquer les membres des associations pour les différentes réunions de MIAGE Connection.
- Le Bureau National doit faire part d’un bilan financier et moral de mi-mandat lors du MIAGE Intensive Camp et d’un bilan financier et moral de fin de mandat lors de l’Assemblée Générale Ordinaire. Ces documents sont envoyés (en prévisionnel) en même temps que la convocation aux assises. Ces bilans seront votés par les administrateurs.
### Article A.4 – Les démissions
La démission d’un membre du Bureau National sera soumise au vote lors de la prochaine assemblée générale. Le Bureau peut nommer un étudiant ou un diplômé de la filière MIAGE pour suppléer aux fonctions du membre démissionnaire jusqu’à la prochaine Assemblée Générale. Celui-ci devient chargé de mission jusqu’à la tenue de l’Assemblée Générale.
### Article A.5 – Les attributions du Président
Le Président :
- Représente l’association MIAGE Connection dans tous les actes de la vie civile ;
- Peut ester en justice en attaque comme en défense au nom de l’association MIAGE Connection pour la défense des intérêts de ses membres, pour la réalisation de l’objet mentionné dans les présents statuts ; après en avoir dûment informé le bureau ; l’Assemblée Générale sera informée à sa plus proche réunion ;
- Est le seul ordonnateur des dépenses avec le Trésorier ;
- Préside et convoque les réunions de bureau, du Conseil d’Administration et de l’Assemblée Générale. Sa voix est prépondérante en cas de partage des voix en réunion de bureau ;
- Le Président peut, en outre, déléguer tout ou partie de ses pouvoirs à un autre membre du bureau pour une durée déterminée ne pouvant excéder la fin de son mandat. La procuration écrite devra être remise au Secrétaire Général.
### Article A.6 – Les attributions du ou des Vice-Présidents
Les Vice-Présidents:
- Doivent suppléer le Président en cas de vacance de son poste ;
- Prennent en charge la réalisation des projets définis par le Conseil d’Administration.
### Article A.7 – Les attributions du Trésorier
Le Trésorier :
Doit tenir les comptes et la trésorerie de l’association,
Doit présenter le bilan financier de l’association lors de l’Assemblée Générale en session ordinaire.
Doit présenter un budget prévisionnel lors de la première assise convoquée par le Bureau National
Doit présenter lors d’un événement, le budget prévisionnel et réel de l’événement précédent.
### Article A.8 – Les attributions du Secrétaire Général
Le Secrétaire Général :
- Est en charge de la correspondance et des archives ;
- Doit s’assurer du respect des présents statuts et de la tenue du registre ;
- Met en oeuvre les dispositions de déclaration prévues par la loi du 1er juillet 1901 et le décret du 16 août 1901.
 
## Titre 7 – Le Financement
### Article A.1 – Les recettes
Les recettes de l’association MIAGE Connection sont composée des cotisations annuelles, des dons manuels et subventions, en particulier celles de l’État, des collectivités territoriales, des administrations publiques et de toute autre recette autorisée par la loi.
### Article A.2 – La comptabilité
Il est tenu une comptabilité et si nécessaire, une comptabilité par matière, dont il est rendu compte au moins une fois par an lors de l’Assemblée Générale en session ordinaire. Les membres peuvent demander à en être informés lors d’une assise, ils doivent toutefois en faire la demande au moins deux jours avant celle-ci.
## Titre 8 – Modifications des statuts et dissolution
### Article A.1 – La dissolution
La dissolution de l’Association MIAGE Connection doit être prononcée par les deux tiers au moins des membres présents à l’Assemblée Générale, qui nommera un ou plusieurs liquidateurs nommés par l’Assemblée Générale. L’actif potentiel est dévolu conformément à l’article 9 de la loi du 1er juillet 1901 et au décret du 16 août 1901.
### Article A.2 – Le règlement intérieur
Un règlement intérieur est établi par le Conseil d’Administration à la majorité simple afin de déterminer toutes les questions n’ayant pas été traitées par les présents statuts et ayant trait à l’organisation et au fonctionnement de l’association MIAGE Connection.
### Article A.3 – Modification des statuts ou du règlement intérieur
Toute modification des statuts de l’association ou du règlement intérieur doit être soumise et votée lors de l’Assemblée Générale à la majorité absolue des membres votants. Les statuts acceptés sont adoptés 10 jours après la tenue du vote.

## Historique des statuts :
- Statuts adoptés à Créteil, lors de l’Assemblée Générale Constitutive du 17 juillet 2004.
- Statuts modifiés à Orléans lors de l’Assemblée Générale du 28 mars 2009.
- Statuts modifiés à Rennes lors de l’Assemblée Générale du 21 novembre 2009.
- Statuts modifiés à Aix lors de l’Assemblée Générale du 12 mai 2010.
- Statuts modifiés à Lille lors de l’Assemblée Générale du 15 mai 2012.
- Statuts modifiés à Grenoble lors de l’Assemblée Générale du 26 avril 2014.
- Statuts modifiés à Nantes lors de l’Assemblée Générale du 25 octobre 2014.
- Statuts modifiés à Rennes lors de l’Assemblée Générale du 25 octobre 2015.
- Statuts modifiés à Laparade lors de l’Assemblée Générale du 05 mars 2017.
- Statuts modifiés à Chevillon lors de l’Assemblée Générale du 11 mars 2018.
- Statuts modifiés à Talence lors de l’Assemblée Générale du 21 octobre 2018.
- Statuts modifiés à Laparade lors de l’Assemblée Générale du 03 mars 2019.
- Statuts modifiés à Saint-Martin-d’Hères lors de l’Assemblée Générale du 05 mai 2019.



Margaux Lequertier, présidente						

Antoine Carrier, secrétaire général 			     			  