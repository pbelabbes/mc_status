# Règlement Intérieur de l'Association 
## Article 1 – Définitions 
- **Etudiant** : sont étudiants les personnes régulièrement inscrites ou immatriculées 
dans un établissement d'enseignement supérieur en vue d'obtenir un diplôme. 
- **Association Etudiante** : une association étudiante est une association légalement 
constituée dont au moins les 3/4 des membres sont étudiants, ainsi que le président. 
- **Association d’Anciens** : une association d’anciens étudiants est une association 
légalement constituée dont au moins les 3/4 des membres sont d’anciens étudiants, 
ainsi que le président. 
- **Administrateur** : un administrateur est soit le Président d’une association adhérente 
à MIAGE Connection soit son représentant temporaire lors d’une assise. 
- **Fédération** : Une fédération est considérée comme valablement constituée si elle 
fédère au moins 5 associations étudiantes en activité, si elle a un président 
valablement élu et si elle a, elle-même, une activité avérée. 
- **Chargé de mission** : Le chargé de mission est une personne chargée d’un dossier 
particulier ou d’une mission par le Bureau National de MIAGE Connection pour une 
durée déterminée. Un chargé de mission ne peut communiquer au nom de MIAGE 
Connection dans les instances extérieures. Le chargé de mission est élu par le CA sur 
proposition du bureau. 
- **Assise** : est appelée assise, toute assemblée qui réunit les administrateurs de MIAGE Connection et son Bureau National, à savoir** : les Conseils d’Administration et les 
Assemblées Générales. 
- **Elu étudiant** : est appelé élu étudiant, tout étudiant en MIAGE élu dans les instances 
universitaires (conseils centraux, conseils d’UFR et conseils de composante) ainsi que 
le CROUS en tant que titulaire ou suppléant. 
- **Membre adhérent permanent** : est appelé membre adhérent permanent, toute personne morale représentant un groupe d’étudiants ou diplômés MIAGE, ayant droit de participer aux assises et au vote lors de ces dernières sans avoir besoin de payer une adhésion annuelle. Ce statut est donné aux organes qui ont au moins une personne qui les représente et qui ont une voix unique pour représenter tout l’organe.
- **Évènement** : est désigné évènement tout rassemblement organisé par MIAGE 
Connection ayant pour but de rassembler ses administrateurs. 
- **Comité d’Orientation Stratégique** : est un organe consultatif composé de trois membres
physiques soumis au vote du Conseil d’Administration, ayant pour but d’accompagner le Bureau National dans une démarche qualité, de pérennisation et d’évolution des actions de la
fédération.
## Article 2 – Montant des cotisations 
La cotisation des membres du collège A est fixée à 50 Euros. 

La cotisation des membres du collège B est fixée à 25 Euros. 

La cotisation sera payée par tout format accepté par le Trésorier de MIAGE 
Connection. 
## Article 3 – Admission d’un nouveau membre 
Pour devenir membre de l’association, à l’exception du collège C, il faut déposer une
demande écrite auprès du Bureau National de MIAGE Connection qui émet un avis et le
transmet lors d’une l’Assemblée Générale. Celle-ci doit comporter : 
- Un exemplaire de ses statuts, son numéro d’enregistrement en préfecture ou au tribunal d’instance pour les associations d’Alsace Moselle,
- La liste du Bureau ou des dirigeants en exercice de la structure associée, 
Une lettre du Président ou dirigeant principal de la structure motivant sa demande. 
- Le Bureau National est chargé de vérifier la présence de toutes les pièces et la conformité
des statuts de la structure avec ceux de MIAGE Connection. II est en droit de prendre
toutes les mesures pour vérifier la validité des pièces fournies. 

Après avoir formulé un avis sur l’admission, le Bureau National soumet la demande lors
de cette Assemblée Générale pour l’admission comme membre associé à la majorité 
qualifiée des votes. 

Pour devenir membre du collège C, il faut déposer une candidature auprès du Bureau
National. Pour que celle-ci soit recevable il faut :
- Une lettre de candidature contenant les motivations de l’élu ainsi qu’une proposition de projet qu’il souhaite porter
- Le justificatif du mandat d’élu

Le Bureau National, après vérification de sa validité, soumet cette candidature au vote du 
Conseil d’Administration.

Tout membre postulant doit pouvoir répondre aux questions qui lui seront soumises lors 
du vote.

## Article 4 – Congrès d’Hiver 
Le Congrès d’Hiver doit avoir lieu annuellement entre le 1er Janvier de l’année N et 
le 15 Mars l’année N. Le président de séance du Congrès d’Hiver est le président de 
MIAGE Connection. L’élection du nouveau Bureau National suit, dans l’ordre du jour, 
la démission du Bureau National sortant. Cette élection est présidée par une 
personne désignée au début du Congrès National. Elle sera secondée par des 
assesseurs. Chaque liste candidate désigne un assesseur. Une fois élu, le nouveau 
Président préside la fin du Congrès National. 
## Article 5 – Bureau National 
Le nombre et les attributions des Vice-Présidents sont définis au moment de 
l’élection du Bureau par la liste qui se présente. 

Le Bureau National peut exclure l’un de ses membres, excepté le Président, sur 
proposition du Président, à la majorité des membres du Bureau National. Cette 
décision peut être remise en cause, sur demande de l’exclu, par le Conseil 
d’Administration suivant. 

Le Bureau National peut coopter un nouveau membre sur proposition du Président. 
Cette cooptation doit être ratifiée par le Conseil d’Administration suivant. Jusqu’à 
ce que la cooptation soit approuvée par le Conseil d’Administration, le nouveau 
membre n’a qu’une voix consultative au Bureau National. Les membres du Bureau 
National sont soumis au devoir de confidentialité et ce y compris après leur mandat. 
## Article 6 – Election du Bureau National 
Les listes de candidats doivent être déposées ou reçues par le Secrétaire Général, au 
plus tard vingt jours avant l’Assemblée Générale du Congrès. Chaque liste doit être 
accompagnée d’une profession de foi ainsi qu’une présentation des missions qui 
seront attribuées à chaque candidat durant son mandat. Ces listes et leurs 
professions de foi doivent être envoyées par courrier électronique aux membres, au 
plus tard quinze jours avant l’Assemblée Générale du Congrès, par le Secrétaire 
Général. Des compléments peuvent être apportés aux listes, au-delà. Auquel cas, 
ces compléments sont annoncés par la tête de liste, candidat au poste de Président, 
au moment de la présentation des listes, pendant l’Assemblée Générale. 

Après son élection, le bureau national fait amender la politique générale présentant 
des objectifs concrets et chiffrés, qu’il communiquera au plus tard quinze jours 
avant la première assise du mandat aux administrateurs afin qu’ils puissent y 
apporter des amendements aussi. La politique générale et ses amendements sont 
soumis au vote lors de cette assise. 

Dans le cas où aucune liste n’obtiendrait la majorité requise par les statuts au 
premier tour, des listes différentes peuvent éventuellement fusionner. 

## Article 7 – Procédure pour les motions 
Lors des assises, toutes les motions et amendements doivent avoir un membre 
porteur de projet et doivent être secondés par un autre membre. Si la motion est 
proposée par le Bureau National elle n’a pas besoin d’être secondée. 

Une motion est rédigée et présentée au Secrétaire Général par un membre, elle est 
alors lue devant le Conseil d’Administration ou l’Assemblée Générale. Le Président 
appelle alors un membre volontaire du Conseil d’Administration ou de l’Assemblée 
Générale pour seconder la motion. S’il n’y en a pas, la motion échoue 
automatiquement. Après avoir proposé une motion, le porteur de projet a le droit de 
parole pour motiver sa motion. 
Ensuite, la motion est ouverte au débat et aux amendements, le porteur de projet 
peut exercer son droit de réponse avant que la motion ne soit soumise au vote. 

Quand un amendement est apporté à une motion, l’amendement doit être voté en 
premier sauf s’il est accepté par les porteurs de la motion, il est alors intégré 
directement à cette dernière. Quand deux amendements ou plus sont proposés, le 
Conseil d’Administration ou l’Assemblée Générale devra voter les amendements par 
ordre décroissant de changements impliqués. Les amendements sont discutés et 
modifiés en respectant la procédure pour les motions. Une motion peut être 
considérée comme un amendement à une motion seulement si elle ajoute, modifie 
ou efface une partie de cette motion. 
Un membre votant en désaccord avec une décision prise lors d’un CA peut avoir sa 
position notée sur le compte rendu de cette séance. 
## Article 8 – Déroulement d’un Conseil d’Administration à distance 
### Modalités
 La convocation de ce type de Conseil d’Administration sera adressée par voie électronique 4 jours à l’avance à l’ensemble des membres du Conseil d’Administration. Les motions à valider sont présentées en pièce jointe à l’ordre du jour, accompagnées de tout document utile à la compréhension du dossier. 

Les collèges B et C  ont 2 jours, après l’envoi de la convocation, pour demander le droit de vote dans les mêmes conditions que dans une assise ordinaire. Dans le cas d’un vote préliminaire, les administrateurs du collège A expriment leurs votes concernant la demande du collège B et/ou du collège C et la motion soumise au vote durant le conseil d’administration à distance. 

Les membres du collège B ainsi que le représentant du collège C votent pendant la période du conseil d’administration à distance. Si un vote préliminaire devait avoir lieu,leurs votes ne seront pris en compte que si le collège A accepte la demande. 

### Procédure de vote 
Chaque membre votant devra envoyer un unique courriel au secrétaire général présenté de la façon suivante pour chaque motion passée au vote : 

**Nom de la motion – Justification de vote (optionnel) et vote (NPPV, Abstention, Pour ou Contre).** 

Les votes seront systématiquement enregistrés et envoyés immédiatement aux administrateurs dès le résultat. 

Le vote se déroule sur 24 heures, de 12h00 (heure de paris) le jour précisé sur la convocation, à 11h59 le lendemain.
## Article 9 – Modalités de remboursement du transport pour les assises de MIAGE Connection 
### Article 9.1 - Associations adhérentes 
Chaque structure adhérente à jour de sa cotisation peut bénéficier du 
remboursement d’un participant pour les frais imputés au transport selon les 
conditions suivantes : 
- La demande de remboursement est envoyée au domicile du trésorier par 
courrier ou e-mail 15 jours maximum après la clôture de l’assise. 
- Les remboursements seront effectifs 30 jours maximum après réception de 
la demande. 
- La demande de remboursement comporte les pièces suivantes : pièces 
justificatives et fiche de remboursement dûment remplie, datée et signée 
par le demandeur. Il est également possible d’envoyer la demande de 
remboursement par courriel en numérisant les pièces nécessaires. 
- Le remboursement ne peut excéder la base de remboursement. 

En outre, en cas de non-présence ou de comportement inapproprié lors des assises et des formations obligatoires, le Bureau National de MIAGE Connection se réserve le droit de ne pas rembourser toute ou partie de la structure représentée par le participant. Dans ce cas un compte-rendu sera présenté aux administrateurs. 

### Article 9.2 - Membres du Bureau National 
Chaque membre du bureau national de MIAGE Connection (hors chargés de mission) 
peut bénéficier d’un remboursement pour les frais imputés au transport selon les 
conditions suivantes : 
- La demande de remboursement est envoyée au domicile du trésorier 
par courrier ou e-mail 15 jours maximum après la clôture de l’assise. 
- Les remboursements seront effectifs 30 jours maximum après réception 
de la demande. 
- La demande de remboursement comporte les pièces suivantes : pièces 
justificatives et fiche de remboursement dûment remplie, datée et 
signée par le demandeur. 
- Le remboursement ne peut excéder 75% de la base de remboursement 
sous réserve d’acceptation préalable du trésorier. 

Lors de l’assemblée générale ordinaire (AGO), les membres du bureau national sortant 
et les membres du nouveau bureau national seront remboursés dans une proportion 
équivalente à la moitié du remboursement habituel. 
### Article 9.3 - Membres du collège C 
Chaque membre du collège C peut bénéficier d’un remboursement pour les frais imputés au transport selon les conditions suivantes : 
- La demande de remboursement est envoyée du trésorier par e-mail 30 jours maximum après la clôture de l’assise. 

- Les remboursements seront effectifs 30 jours maximum après réception de la demande. 
- La demande de remboursement comporte les pièces suivantes : pièces justificatives et fiche de remboursement dûment remplie, datée et signée par le demandeur. 
- Le remboursement ne peut excéder 25% de la base de remboursement sous réserve d’acceptation préalable du trésorier. 
- Le collège C doit effectuer un compte-rendu de son travail durant l’assise de l’événement. 

En outre, en cas de non-présence lors des assises ou des formations  dispensées au collège C (et aux élus), sans dérogation accordée de la part du pôle en charge des élus, ou de comportement inapproprié, le Bureau National de MIAGE Connection se réserve le droit de ne pas rembourser la personne. Dans ce cas un compte-rendu sera présenté aux administrateurs. 
### Article 9.4 - Chargés de missions 
Chaque chargé de mission du bureau national de MIAGE Connection peut bénéficier 
d’un remboursement pour les frais imputés au transport selon les conditions 
suivantes : - La demande de remboursement est envoyée au trésorier par e-mail 
15 jours maximum après la clôture de l’assise. 
- Les remboursements seront effectifs 30 jours maximum après 
réception de la demande. 
- La demande de remboursement comporte les pièces suivantes : 
pièces justificatives et fiche de remboursement dûment remplie, datée et 
signée par le demandeur. 
- Le remboursement ne peut excéder 75% de la base de 
remboursement sous réserve d’acceptation préalable du trésorier. 
- Le chargé de mission doit émettre un compte rendu de son travail 
durant une assise, ou animer un atelier durant l’événement, sa présence est 
donc nécessaire. 

Lors de l’assemblée générale ordinaire(AGO),les chargés de missions sortants et les 
nouveaux chargés de mission seront remboursés dans une proportion équivalente à la 
moitié du remboursement habituel. 
### Article 9.5 - Frais des formateurs MIAGE Connection 
Modalités de remboursement
Chaque membre formateur MIAGE Connection peut bénéficier du remboursement pour les frais imputés au transport pour les déplacements lors des événements de formation de MIAGE Connection, s’il y fait une formation, selon les conditions suivantes :
- Le formateur MIAGE Connection dispense une formation ou anime un atelier durant un événement de formation de MIAGE Connection.
- La demande de remboursement est envoyée au trésorier par e-mail 30 jours maximum après la clôture de l’événement de formation.
- Les remboursements seront effectués 30 jours maximum après réception de la demande.
- La demande de remboursement comporte les pièces suivantes : pièces justificatives originales et fiche de remboursement dûment remplie, datée et signée par le demandeur.
- Le remboursement ne peut excéder 75% de la base de remboursement.

En cas de non-respect de la charte, le bureau national ou les administrateurs peuvent décider du non-remboursement du formateur. 

### Article 9.6 - Base de remboursement 
La base de remboursement a pour but de rembourser les frais liés au déplacement lors d’un évènement organisé par MIAGE Connection comportant une assise. 
La formule à appliquer est la suivante : P taux coef R = * 

\* où : 
- R est le remboursement, 
- P est le prix d’un billet seconde classe plein tarif loisir, entre la gare la plus proche de l’adresse de domiciliation de l’association et la gare la plus proche de la ville du lieu de l’assise, décrit dans les conditions générales de vente SNCF dans la dernière édition des conditions de ventes SNCF, 
- taux est le taux de remboursement du RI, 
- coef est le coefficient, par défaut égal à 1, mais le trésorier, après concertation avec le bureau, peut faire varier ce coefficient entre 0 et 1. Cette mesure permet une flexibilité des remboursements afin de prévenir tout souci éventuel. 

Le train est le moyen de transport par défaut, un autre moyen de transport pourra être utilisé si le train n’est pas une alternative envisageable selon certains des critères ci-dessous : 
- Aucun départ n’est prévu après 17h, la veille de l’ouverture de 
l’évènement, 
- le temps de trajet est supérieur à 7h, 
- le nombre de correspondances est supérieur à 3, 
- l’arrivée ne s’effectue pas avant l’ouverture de l’évènement, 
- la ville du lieu de l’assise n’est pas desservie par une gare. 

Le montant maximum qui sera remboursé sera celui indiqué dans la base tarifaire prévue. 

Les frais de taxi, de VTC, de parking, et les contraventions ne seront pas pris en compte, sauf situation exceptionnelle. 

Le moyen de transport pour une correspondance obligatoire sur le trajet sera remboursé après émission de justificatifs et sur appréciation du trésorier. 

Chaque demande de remboursement devra être envoyée au plus tard quatre semaines après l’assise, complète et avec la copie des justificatifs. Ce remboursement sera ensuite effectué sous 30 jours après réception de la demande. 

La base de remboursement sera communiquée dès le premier jour de l’ouverture des inscriptions. 

Dans le cas où le coefficient est inférieur à 1, le bureau national doit motiver son choix auprès des administrateurs en communiquant : 
1. Un tableau comparatif par ville comportant : 

   a. Le montant du remboursement au coefficient choisi 
   
   b. Le montant du remboursement avec un coefficient de 1 
2. Un document explicatif comportant : 

   a. L’ensemble des faits retenus pour le choix du coefficient 
   b. Le solde du compte en banque 

Une échelle à titre indicatif, définie par le trésorier et le bureau national, s’applique au coefficient en fonction de la santé financière de MIAGE Connection : 
- 0 à 0.3 pour une santé financière dangereuse, 
- 0.3 à 0.6 pour une santé financière préoccupante, 
- 0.6 à 1 pour une santé financière viable. 
## Article 10 – Frais exigibles pour les participants aux assises de MIAGE Connection 
### Détermination du montant de la participation 
Le montant de la participation est composé de quatre paramètres : 
- Les frais de restauration 
- Les frais d’hébergement 
- Les frais de transport en commun 
- Les frais annexes 

Tous les participants sont à même de payer leur inscription à un événement organisé 
par MIAGE Connection. 
Ces quatre paramètres définissent le montant maximal de l’inscription. Le tarif final 
reste à l’appréciation du trésorier de MIAGE Connection en fonction de la santé 
financière de la fédération. 

### Frais de restauration 
Le financement des repas du soir est à la charge de tous les participants. 

Le financement des petits déjeuners et des déjeuners est assuré par MIAGE 
Connection. 

### Frais d’hébergement 
Le financement de l’hébergement des participants lors des assises est assuré par 
MIAGE Connection. 

### Frais de transport en commun 
Le financement des frais de transport en commun obligatoires lors du déroulement 
de l’assise sont assurés par MIAGE Connection. 

Le Bureau National peut fournir des titres de transport ou demander aux participants 
des preuves d’achat lors de la phase de remboursement, sur appréciation du 
trésorier de MIAGE Connection. 

### Marge de sécurité 
La marge de sécurité permet de prendre en compte le risque de dépassement 
d’enveloppe budgétaire et de limiter les pertes sur un événement. 

### Frais annexes 
Le financement des frais annexes inhérents au déroulement de l’assise sont assurés 
par MIAGE Connection. 
## Article 11 – Modalités d’inscriptions pour les participants aux assises 
### Lancement des inscriptions 
La date, la ville et la procédure d’inscription pour l’assise devront être communiquées au 
moins cinq semaines à l’avance aux administrateurs, sauf exceptions à l’appréciation du 
Bureau National. 

### Quota 
Chaque membre adhérent bénéficie de plein droit d’une place réservée au porteur du droit 
d’administration pour assister aux assises de MIAGE Connection. Le Bureau National fixe 
par la suite le nombre de places par membre adhérent  en fonction de  l’enveloppe 
budgétaire allouée dans le budget prévisionnel annuel pour l’assise. 

### Délai d’inscription
Les membres adhérents doivent inscrire les participants au plus tard deux semaines avant 
le début de l’assise. Le Bureau National a le droit de refuser une inscription au-delà de
cette date limite. 

### Redistribution de quota 
Les places non utilisées qui sont réservées initialement aux membres adhérents pourront
alors être redistribuées sous réserve d’une justification recevable à l'appréciation du 
Bureau National. La redistribution doit être équitable entre les associations adhérentes 
ayant la même justification. La priorisation des justifications est à l’appréciation du Bureau National. 

### Annulation d’un participant 
Les personnes annulant leur inscription moins de deux semaines avant l’événement ne 
seront pas remboursées, sauf exceptions à l'appréciation du Bureau National.

## Article 12 : Admission d'un formateur MIAGE Connection
Sont formateurs MIAGE Connection, toutes personnes physique étudiant ou diplômés de 
la filière MIAGE ayant : 
- Signé la charte des formateurs de MIAGE Connection,
- Reçu une attestation formateur MIAGE Connection. 

Ils ne doivent pas s’acquitter de la cotisation annuelle. 

Les formateurs peuvent perdre ce statut :
- Par demande du formateur 
- Par décision de la majorité des formateurs
- Par vote du CA 

L'attestation est donné par le(s) responsable(s) de formation du bureau national, lors 
d’une assise, suite à la décision prise par les formateurs. 

Dans le cas où le nombre de formateurs est inférieur à 3, le(s) responsable(s) de 
formation du bureau national pourra/pourront décider de remettre des attestations.  


## Article 13 : Événement de passation 
L’événement de passation annuel de MIAGE Connection est assujetti à un 
remboursement à hauteur de 75% de la base de remboursement, dans la limite de 
1500€, si les conditions suivantes sont respectées : 
- Les administrateurs ont voté pour son remboursement lors de la 
dernière assise. 
- L’accord du trésorier sortant a été donné. 
Les personnes concernées par ce remboursement sont les membres entrants et 
sortants du bureau national de MIAGE Connection. 
## Article 14 : Événement bureau national 
Les événements du bureau national sont des événements où le bureau se retrouve 
pour travailler les problématiques de MIAGE Connection. Il est assujetti à un 
remboursement de 25% de la base de remboursement si les conditions suivantes sont 
respectées : 
- Les administrateurs ont voté pour son remboursement lors de l’assise 
suivante. 
- L’accord du trésorier a été donné. 
Le nombre d’événements remboursés ne peut dépasser 2 par an, le budget maximum 
de remboursement est de 500€ par événement. 



## Article 15 : Choix des dates et lieux des événements 
Les dates et lieux des événements MIAGE Connection seront votés lors des 
événements suivants : 
- le MIAGE Intensive Camp voté lors du Spring Connection 
- le Congrès d’Hiver voté lors du MIAGE Intensive Camp 

et par CA à distance : 
- le Spring Connection voté entre le Winter Connection et le Congrès d’Hiver 
- le Winter Connection voté entre le Spring Connection et le MIAGE Intensive Camp. 

## Article 16 - Procédure de candidature à l’organisation d’un événement MC 
Trente jours avant le vote du lieu d’un événement, MIAGE Connection lance un appel 
à candidature. Les villes souhaitant accueillir l’événement pourront dès alors se 
manifester auprès de MIAGE Connection et ce, jusqu’à la date du vote. Les villes 
souhaitant appuyer leur candidature à l’aide d’un dossier devront le faire parvenir à 
MIAGE Connection au plus tard 5 jours avant la date du vote, afin que MIAGE 
Connection puisse le transmettre aux administrateurs. 

Dans le cas contraire, la ville devra communiquer son dossier et/ou sa candidature par ses propres moyens.


## Article 17 - Les groupes de travail 
### Fonction du groupe de travail 
Un groupe de travail est un espace créé par MIAGE Connection ou par les administrateurs afin de collaborer sur des sujets qu'ils souhaitent développer dans un temps donné. 

### Constitution du groupe de travail 
Pour créer un groupe de travail il faut : 
- nommer un leader : c’est lui qui organisera les réunions pour travailler sur les sujets et qui communiquera les comptes-rendus 
- une équipe : toute personne motivée à y contribuer 
- un membre du Bureau National qui assurera le suivi : s'assurer que le groupe avance et faire le lien avec le Bureau National 
- une deadline 

## Article 18 - Les missions du Comité d’Orientation Stratégique

Les missions du Comité d’Orientation Stratégique de MIAGE Connection sont définies
dans le document “Les missions du Comité d’Orientation Stratégique de MIAGE
Connection”.

Toute modification des missions est présentée au Conseil d’Administration par le Bureau 
National.

## Historiques des modifications 
- Règlement Intérieur adopté à Orléans lors de l’Assemblée Générale du 28 mars 2009. 
- Règlement Intérieur modifié à Rennes lors de l’Assemblée Générale du 21 novembre 2009. 
- Règlement Intérieur modifié à Créteil lors de l’Assemblée Générale du 20 novembre 2010. 
- Règlement Intérieur modifié à Grenoble lors de l’Assemblée Générale du 26 avril 2014. 
- Règlement Intérieur modifié à Paris lors de l’Assemblée Générale du 7 décembre 2014. 
- Règlement Intérieur modifié à Nanterre lors de l’Assemblée Générale du 26 mars 2015. 
- Règlement Intérieur modifié à Rennes lors de l’Assemblée Générale du 25 octobre 2015. 
- Règlement Intérieur modifié à Laparade lors de l’Assemblée Générale du 21 février 2016. 
- Règlement Intérieur modifié à Amiens lors de l’Assemblée Générale du 1er mai 2016. 
- Règlement Intérieur modifié à Nancy lors de l’Assemblée Générale du 11 décembre 2016. 
- Règlement Intérieur modifié à Laparade lors de l’Assemblée Générale du 4 mars 2017. 
- Règlement Intérieur modifié à Nanterre lors de l’Assemblée Générale du 23 Avril 2017. 
- Règlement Intérieur modifié à Aix-en-Provence lors de l’Assemblée Générale du 22 octobre 2017. 
- Règlement Intérieur modifié à Chevillon lors de l’Assemblée Générale du 11 mars 2018. 
- Règlement Intérieur modifié à Talence lors de l’Assemblée Générale du 21 octobre 2018. 
- Règlement Intérieur modifié à Laparade lors de l’Assemblée Générale du 03 mars 2019.
- Règlement Intérieur modifié à Saint-Martin-d’Hères lors de l’Assemblée Générale du 05 mai 2019. 
